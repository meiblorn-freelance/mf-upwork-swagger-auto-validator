package com.openapi.validation.domain.config;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Endpoint definition
 */
public class EndpointDefinition {

    @JsonProperty(value = "path", required = true)
    private String path;

    @JsonProperty(value = "operations", required = true)
    private Map<HttpMethodDefinition, OperationDefinition> operations = new HashMap<>();

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Map<HttpMethodDefinition, OperationDefinition> getOperations() {
        return operations;
    }

    public void setOperations(Map<HttpMethodDefinition, OperationDefinition> operations) {
        this.operations = operations.entrySet().stream()
                                    .sorted(Comparator.comparingInt(o -> o.getValue().getOrder()))
                                    .collect(
                                            Collectors.toMap(
                                                    Map.Entry::getKey,
                                                    Map.Entry::getValue,
                                                    (e1, e2) -> e1, LinkedHashMap::new
                                            )
                                    );
    }

    @Override
    public String toString() {
        return "EndpointDefinition{" +
                "path='" + path + '\'' +
                ", operations=" + operations +
                '}';
    }
}
