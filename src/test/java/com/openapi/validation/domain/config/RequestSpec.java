package com.openapi.validation.domain.config;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Request specs (requirements)
 */
@SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
public class RequestSpec {

    @JsonProperty("headers")
    private Map<String, String> headers = new HashMap<>();

    @JsonProperty("contentType")
    private String contentType;

    @JsonProperty("pathParams")
    private Map<String, String> pathParams = new HashMap<>();

    @JsonProperty("queryParams")
    private Map<String, Object> queryParams = new HashMap<>();

    @JsonProperty("body")
    private Object body;

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Map<String, String> getPathParams() {
        return pathParams;
    }

    public void setPathParams(Map<String, String> pathParams) {
        this.pathParams = pathParams;
    }

    public Map<String, Object> getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(Map<String, Object> queryParams) {
        this.queryParams = queryParams;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "RequestSpec{" +
                "headers=" + headers +
                ", contentType='" + contentType + '\'' +
                ", pathParams=" + pathParams +
                ", queryParams=" + queryParams +
                ", body=" + body +
                '}';
    }
}
