package com.openapi.validation.domain.config;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Operation specs: requirements
 */
public class OperationSpec {

    @JsonProperty(value = "request")
    private RequestSpec request;

    @JsonProperty("response")
    private ResponseSpec response;

    public RequestSpec getRequest() {
        return request;
    }

    public void setRequest(RequestSpec request) {
        this.request = request;
    }

    public ResponseSpec getResponse() {
        return response;
    }

    public void setResponse(ResponseSpec response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "OperationSpec{" +
                ", request=" + request +
                ", response=" + response +
                '}';
    }
}
