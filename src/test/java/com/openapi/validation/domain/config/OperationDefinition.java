package com.openapi.validation.domain.config;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Operation definition
 */
public class OperationDefinition {

    @JsonProperty(value = "order")
    private Integer order = -1;

    @JsonProperty("specs")
    private List<OperationSpec> specs = new ArrayList<>();

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<OperationSpec> getSpecs() {
        return specs;
    }

    public void setSpecs(List<OperationSpec> specs) {
        this.specs = specs;
    }

    @Override
    public String toString() {
        return "OperationDefinition{" +
                "order=" + order +
                ", specs=" + specs +
                '}';
    }

}
