package com.openapi.validation.domain.config;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.StringUtils;

/**
 * Http method definitions used in config
 */
public enum HttpMethodDefinition {
    POST("post"),
    GET("get"),
    PUT("put"),
    PATCH("patch"),
    DELETE("delete"),
    HEAD("head"),
    OPTIONS("options"),
    TRACE("trace");

    private static final Map<String, HttpMethodDefinition> VALUE_MAP = new HashMap<>();

    static {
        for (HttpMethodDefinition value : HttpMethodDefinition.values()) {
            VALUE_MAP.put(value.lower, value);
        }
    }

    private final String lower;

    HttpMethodDefinition(String lower) {
        this.lower = lower;
    }

    @JsonCreator
    public static HttpMethodDefinition forValue(String value) {
        return VALUE_MAP.get(StringUtils.lowerCase(value));
    }

    @JsonValue
    public String getLower() {
        return lower;
    }

}
