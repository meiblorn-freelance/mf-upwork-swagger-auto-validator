package com.openapi.validation.domain.config;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Response specs (requirements)
 */
public class ResponseSpec {

    /**
     * Acceptable HTTP statuses (additional to 200 and 201 from
     *
     * @see com.openapi.validation.OpenApiValidationConstants#ALLOWED_STATUS_CODES)
     */
    @JsonProperty("status")
    private List<Integer> status = new ArrayList<>();

    public List<Integer> getStatus() {
        return status;
    }

    public void setStatus(List<Integer> status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ResponseSpec{" +
                "status=" + status +
                '}';
    }
}
