package com.openapi.validation.domain.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Config holder
 */
public class Config {

    @JsonProperty(value = "url", required = true)
    private String url;

    @JsonProperty("servers")
    private List<String> servers = new ArrayList<>();

    @JsonProperty("endpoints")
    private List<EndpointDefinition> endpoints = new ArrayList<>();

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getServers() {
        return servers;
    }

    public void setServers(List<String> servers) {
        this.servers = servers;
    }

    public List<EndpointDefinition> getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(List<EndpointDefinition> endpoints) {
        this.endpoints = endpoints;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Config{" +
                "url='" + url + '\'' +
                ", servers=" + servers +
                ", endpoints=" + endpoints +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
