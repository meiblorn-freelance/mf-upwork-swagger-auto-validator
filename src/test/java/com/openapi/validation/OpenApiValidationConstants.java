package com.openapi.validation;

import java.util.Arrays;
import java.util.List;

/**
 * Global constants class
 */
public class OpenApiValidationConstants {

    /**
     * Environment variable to override config path
     */
    public static final String CONFIG_PATH_ENV_KEY = "openapi.validation.config";

    /**
     * Default config path (it is located in tests resources directory)
     */
    public static final String CONFIG_PATH_DEFAULT = "config.json";

    /**
     * Trusted/allowed HTTP status codes which used by default to validate response
     */
    public static final List<Integer> ALLOWED_STATUS_CODES = Arrays.asList(200, 201);

    /**
     * Use this placeholder in config.json specs body/query strings/headers/path params to generate them randomly.
     * Example:
     *
     *     {
     *       "path": "/user/createWithList",
     *       "operations": {
     *         "post": {
     *           "specs": [
     *             {
     *               "request": {
     *                 "body": [
     *                   {
     *                     "firstName": "FirstName",
     *                     "lastName": "<<rand>>"
     *                   }
     *                 ]
     *               }
     *             }
     *           ]
     *         }
     *       }
     *     }
     */
    public static final String RANDOM_DATA_PLACEHOLDER = "<<rand>>";

    /**
     * Use this placeholder with multipart/* content type to send file to server.
     * Example:
     *
     *    {
     *       "path": "/pet/{petId}/uploadImage",
     *       "operations": {
     *         "post": {
     *           "specs": [
     *             {
     *               "request": {
     *                 "contentType": "multipart/form-data",
     *                 "pathParams": {
     *                   "petId": 1
     *                 },
     *                 "body": {
     *                   "file": "<<file>>dog.jpg"
     *                 }
     *               }
     *             }
     *           ]
     *         }
     *       }
     *     }
     */
    public static final String FILE_DATA_PLACEHOLDER = "<<file>>";
}
