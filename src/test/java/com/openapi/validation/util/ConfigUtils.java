package com.openapi.validation.util;

import static com.openapi.validation.OpenApiValidationConstants.CONFIG_PATH_DEFAULT;
import static com.openapi.validation.OpenApiValidationConstants.CONFIG_PATH_ENV_KEY;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openapi.validation.domain.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Config utils
 */
public class ConfigUtils {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigUtils.class);

    /**
     * Object mapper suited to parse json files
     */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private ConfigUtils() {
    }

    /**
     * Read config file from the path provided env variable (or from default path as fallback).
     *
     * @return Config instance (object).
     * @throws IOException when config was not found.
     */
    public static Config read() throws IOException {
        String configJsonPath = System.getProperty(CONFIG_PATH_ENV_KEY, CONFIG_PATH_DEFAULT);
        return OBJECT_MAPPER.readValue(FileUtils.load(configJsonPath), Config.class);
    }

}
