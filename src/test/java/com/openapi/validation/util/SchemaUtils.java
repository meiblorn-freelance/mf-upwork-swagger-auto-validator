package com.openapi.validation.util;

import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.BooleanSchema;
import io.swagger.v3.oas.models.media.DateTimeSchema;
import io.swagger.v3.oas.models.media.IntegerSchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.StringSchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Schema utils
 */
@SuppressWarnings("WeakerAccess")
public class SchemaUtils {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SchemaUtils.class);

    public SchemaUtils() {
    }

    /**
     * Check is string schema or not.
     *
     * @param schema OpenApi schema.
     * @return yes or no.
     */
    public static boolean isStringSchema(Schema schema) {
        return schema instanceof StringSchema || "string".equals(schema.getType());
    }

    /**
     * Check is integer schema or not.
     *
     * @param schema OpenApi schema.
     * @return yes or no.
     */
    public static boolean isIntegerSchema(Schema schema) {
        return schema instanceof IntegerSchema || "integer".equals(schema.getType());
    }

    /**
     * Check is boolean schema or not.
     *
     * @param schema OpenApi schema.
     * @return yes or no.
     */
    public static boolean isBooleanSchema(Schema schema) {
        return schema instanceof BooleanSchema;
    }

    /**
     * Check is date time schema or not.
     *
     * @param schema OpenApi schema.
     * @return yes or no.
     */
    public static boolean isDateTimeSchema(Schema schema) {
        return schema instanceof DateTimeSchema;
    }

    /**
     * Check is array schema or not.
     *
     * @param schema OpenApi schema.
     * @return yes or no.
     */
    public static boolean isArraySchema(Schema schema) {
        return schema instanceof ArraySchema;
    }
}
