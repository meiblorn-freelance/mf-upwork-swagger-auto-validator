package com.openapi.validation.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * File utils
 */
@SuppressWarnings("WeakerAccess")
public class FileUtils {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils() {
    }

    /**
     * Loads file from the filesystem (first) or classpath (as fallback).
     *
     * @param path path to file.
     * @return File object.
     * @throws IOException when file was not found.
     */
    public static InputStream load(String path) throws IOException {
        File f = new File(path);

        if (f.isFile()) {
            return new FileInputStream(f);
        }

        ClassLoader classLoader = FileUtils.class.getClassLoader();
        return classLoader.getResourceAsStream(path);
    }

}
