package com.openapi.validation.util;

import java.util.Set;
import java.util.stream.Collectors;

import io.swagger.v3.oas.models.OpenAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * OpenApi utils
 */
public class OpenApiUtils {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenApiUtils.class);

    private OpenApiUtils() {
    }

    /**
     * Lists servers provided by OpenApi specification.
     *
     * @param openApi OpenApi instance.
     * @return servers list.
     */
    public static Set<String> servers(OpenAPI openApi) {
        return openApi.getServers().stream()
                      .map(s -> s.getUrl().replace("https", "http"))
                      .collect(Collectors.toSet());
    }

}
