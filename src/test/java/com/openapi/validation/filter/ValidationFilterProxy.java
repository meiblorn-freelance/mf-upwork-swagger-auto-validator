package com.openapi.validation.filter;

import static java.util.Collections.singletonList;

import com.atlassian.oai.validator.OpenApiInteractionValidator;
import com.atlassian.oai.validator.report.ValidationReport;
import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import com.atlassian.oai.validator.restassured.RestAssuredRequest;
import com.atlassian.oai.validator.restassured.RestAssuredResponse;
import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.filter.rule.StatusCodeValidationFilterRule;
import com.openapi.validation.filter.rule.ValidationFilterErrorRuleChain;
import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RestAssured filter which combines own-written validation
 * and validation performed by "swagger-request-validator-core" library
 */
@SuppressWarnings("WeakerAccess")
public class ValidationFilterProxy implements Filter {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationFilterProxy.class);

    /**
     * OpenApi interaction validator provided by "swagger-request-validator-core" library.
     */
    private final OpenApiInteractionValidator openApiInteractionValidator;

    /**
     * Operation spec (from config.json) used to validate response from the server.
     */
    private final OperationSpec spec;

    /**
     * Error rule chain. It behaves like a workflow and decides accept response or mark it as failed.
     */
    private final ValidationFilterErrorRuleChain ruleChain;

    public ValidationFilterProxy(OpenApiInteractionValidator openApiInteractionValidator, OperationSpec spec) {
        this(
                openApiInteractionValidator, spec,
                new ValidationFilterErrorRuleChain(
                        singletonList(new StatusCodeValidationFilterRule())
                )
        );
    }

    public ValidationFilterProxy(OpenApiInteractionValidator openApiInteractionValidator, OperationSpec spec,
            ValidationFilterErrorRuleChain ruleChain) {
        this.openApiInteractionValidator = openApiInteractionValidator;
        this.spec = spec;
        this.ruleChain = ruleChain;
    }

    /**
     * Filter method.
     *
     * @param filterRequestSpec  RestAssured request specs.
     * @param filterResponseSpec RestAssured response specs.
     * @param filterContext      filter context.
     * @return response from the server.
     */
    @Override
    public Response filter(FilterableRequestSpecification filterRequestSpec,
            FilterableResponseSpecification filterResponseSpec,
            FilterContext filterContext) {

        Response response = filterContext.next(filterRequestSpec, filterResponseSpec);
        ValidationReport validationReport = this.openApiInteractionValidator
                .validate(RestAssuredRequest.of(filterRequestSpec), RestAssuredResponse.of(response));

        if (ruleChain == null || !ruleChain.validate(validationReport, spec)) {
            throw new OpenApiValidationFilter.OpenApiValidationException(validationReport);
        }

        return response;

    }
}
