package com.openapi.validation.filter.rule;

public enum ValidationFilterErrorRuleDecision {
    PASS,
    FATAL,
    IGNORE
}
