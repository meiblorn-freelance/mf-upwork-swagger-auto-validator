package com.openapi.validation.filter.rule;

import static com.openapi.validation.OpenApiValidationConstants.ALLOWED_STATUS_CODES;
import static com.openapi.validation.filter.rule.ValidationFilterErrorRuleDecision.FATAL;
import static com.openapi.validation.filter.rule.ValidationFilterErrorRuleDecision.IGNORE;
import static com.openapi.validation.filter.rule.ValidationFilterErrorRuleDecision.PASS;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.oai.validator.report.ValidationReport.Message;
import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.domain.config.ResponseSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * "swagger-request-validator-core" doesn't accept status codes not listed in swagger scheme.
 * This rule allows to avoid this restriction.
 */
public class StatusCodeValidationFilterRule extends ValidationFilterErrorRule {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(StatusCodeValidationFilterRule.class);

    private static final Pattern MESSAGE_REGEX = Pattern.compile("Response status ([\\d]+) not defined .*");

    private static final String ERROR_CODE = "validation.response.status.unknown";

    @Override
    public ValidationFilterErrorRuleDecision apply(Message message, OperationSpec spec) {
        if (!ERROR_CODE.equals(message.getKey())) {
            return IGNORE;
        }

        String messageText = message.getMessage();
        Matcher matcher = MESSAGE_REGEX.matcher(messageText);
        if (matcher.matches()) {
            Integer status = Integer.parseInt(matcher.group(1));

            if (ALLOWED_STATUS_CODES.contains(status)) {
                return PASS;
            }

            if (spec != null) {
                ResponseSpec responseSpec = spec.getResponse();
                if (responseSpec != null && responseSpec.getStatus().contains(status)) {
                    return PASS;
                }
            }

            return FATAL;
        }

        return FATAL;
    }

}
