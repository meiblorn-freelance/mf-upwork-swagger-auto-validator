package com.openapi.validation.filter.rule;

import java.util.List;

import com.atlassian.oai.validator.report.ValidationReport;
import com.openapi.validation.domain.config.OperationSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Validation rule chain suited to decide is response valid or not.
 */
public class ValidationFilterErrorRuleChain {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationFilterErrorRuleChain.class);

    private List<ValidationFilterErrorRule> rules;

    public ValidationFilterErrorRuleChain(List<ValidationFilterErrorRule> rules) {
        this.rules = rules;
    }

    public boolean validate(ValidationReport validationReport, OperationSpec spec) {
        if (!validationReport.hasErrors()) {
            return true;
        }

        for (ValidationReport.Message message : validationReport.getMessages()) {
            for (ValidationFilterErrorRule rule : rules) {
                if (ValidationFilterErrorRuleDecision.FATAL.equals(rule.apply(message, spec))) {
                    return false;
                }
            }
        }

        return true;
    }

}
