package com.openapi.validation.filter.rule;

import com.atlassian.oai.validator.report.ValidationReport.Message;
import com.openapi.validation.domain.config.OperationSpec;

public abstract class ValidationFilterErrorRule {

    public abstract ValidationFilterErrorRuleDecision apply(Message message, OperationSpec spec);

}
