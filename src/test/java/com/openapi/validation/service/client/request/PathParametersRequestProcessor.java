package com.openapi.validation.service.client.request;

import static com.openapi.validation.factory.schema.OpenApiFakeSubstitutionFactory.substitute;
import static com.openapi.validation.service.client.request.PathParametersRequestProcessor.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.domain.config.RequestSpec;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.parameters.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Processor suited to configure and substitute path parameters of request url (e.g. /{petId}).
 */
public class PathParametersRequestProcessor implements RequestProcessor<Result> {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PathParametersRequestProcessor.class);

    /**
     * Process method.
     *
     * @param path       url path
     * @param op         OpenApi operation
     * @param httpMethod Http method
     * @param spec       Operation specs
     * @param when       request specification root instance reference.
     * @return updated request specification root instance reference as passed to "when" parameters.
     */
    public Result process(String path, Operation op, PathItem.HttpMethod httpMethod, OperationSpec spec,
            RequestSpecification when) {

        if (op.getParameters() == null) {
            return new Result(when);
        }

        Map<String, String> values = values(spec);

        List<String> appliedParameters = new ArrayList<>();
        for (Parameter parameter : op.getParameters()) {
            String parameterName = parameter.getName();
            if (!path.contains('{' + parameterName + '}')) {
                continue;
            }

            if (!values.containsKey(parameterName)) {
                LOGGER.error("No parameter '{}' was defined for '{} {}'", parameterName, httpMethod, path);
                continue;
            }

            when = when.pathParam(
                    parameterName, substitute(parameter.getSchema(), values.get(parameter.getName()))
            );

            appliedParameters.add(parameterName);
        }

        return new Result(appliedParameters, when);
    }

    private Map<String, String> values(OperationSpec spec) {
        Map<String, String> values = new HashMap<>();
        if (spec != null) {
            RequestSpec requestSpec = spec.getRequest();
            if (requestSpec != null) {
                values.putAll(requestSpec.getPathParams());
            }
        }
        return values;
    }

    public static class Result {

        private final List<String> parameters;

        private final RequestSpecification when;

        Result(RequestSpecification when) {
            this(new ArrayList<>(), when);
        }

        Result(List<String> parameters, RequestSpecification when) {
            this.parameters = parameters;
            this.when = when;
        }

        public List<String> getParameters() {
            return parameters;
        }

        public RequestSpecification getWhen() {
            return when;
        }
    }
}
