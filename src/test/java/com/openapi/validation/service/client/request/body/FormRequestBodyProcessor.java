package com.openapi.validation.service.client.request.body;

import static com.openapi.validation.factory.schema.OpenApiFakeSubstitutionFactory.substitute;
import static io.restassured.http.ContentType.URLENC;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.domain.config.RequestSpec;
import com.openapi.validation.factory.schema.OpenApiFakeDataFactory;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * application/x-www-form-urlencoded content type request body preprocessor
 */
public class FormRequestBodyProcessor implements ContentTypeBodyProcessor {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MultiPartBodyProcessor.class);

    /**
     * @return x-www-form-urlencoded content types
     */
    @Override
    public List<String> acceptableContentTypes() {
        return Collections.singletonList(URLENC.getAcceptHeader());
    }

    /**
     * Apply content type to request.
     *
     * @param contentType content type.
     * @param when        request specification.
     * @return updated request specification
     */
    @Override
    public RequestSpecification applyContentType(String contentType, RequestSpecification when) {
        return when.contentType(URLENC);
    }

    /**
     * Processes request to match operation specs from the config.json and openApi specifications.
     *
     * @param mediaType Media type meta from the OpenApi specification
     * @param spec      Operation specs: requirements to request
     * @param when      rest assured request specification.
     * @return updated rest assured request specification.
     */
    @Override
    @SuppressWarnings("unchecked")
    public RequestSpecification process(MediaType mediaType, OperationSpec spec, RequestSpecification when) {
        if (spec == null) {
            return when;
        }

        RequestSpec requestSpec = spec.getRequest();
        Object body;
        if (requestSpec == null || requestSpec.getBody() == null) {
            Schema schema = mediaType.getSchema();
            for (Object objEntry : schema.getProperties().entrySet()) {
                Map.Entry entry = (Map.Entry) objEntry;
                String name = (String) entry.getKey();
                Schema propertySchema = (Schema) entry.getValue();
                when = when.formParam(name, OpenApiFakeDataFactory.fake(propertySchema));
            }
        } else {
            body = requestSpec.getBody();
            if (body instanceof Map<?, ?>) {
                for (Map.Entry<String, Object> entry : ((Map<String, Object>) body).entrySet()) {
                    when = when.formParam(entry.getKey(), substitute(entry.getValue()));
                }
            }
        }

        return when;
    }

}
