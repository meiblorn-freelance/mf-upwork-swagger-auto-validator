package com.openapi.validation.service.client.request;

import static com.openapi.validation.factory.schema.OpenApiFakeSubstitutionFactory.substitute;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.domain.config.RequestSpec;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem.HttpMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Processor suited to form request query parameters string (e.g. ?a=4&b=6).
 */
public class QueryParametersRequestProcessor implements RequestProcessor<RequestSpecification> {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(QueryParametersRequestProcessor.class);

    /**
     * Process method.
     *
     * @param path       url path
     * @param op         OpenApi operation
     * @param httpMethod Http method
     * @param spec       Operation specs
     * @param when       request specification root instance reference.
     * @return updated request specification root instance reference as passed to "when" parameters.
     */
    public RequestSpecification process(String path, Operation op, HttpMethod httpMethod, OperationSpec spec,
            RequestSpecification when) {

        if (spec == null) {
            return when;
        }

        RequestSpec requestSpec = spec.getRequest();
        if (requestSpec == null) {
            return when;
        }

        Map<String, Object> queryParams = requestSpec.getQueryParams();
        if (queryParams == null || queryParams.isEmpty()) {
            return when;
        }

        if (op.getParameters() == null) {
            return when;
        }

        for (Map.Entry<String, Object> queryParam : queryParams.entrySet()) {
            Object value = queryParam.getValue();
            if (value instanceof String) {
                value = substitute(value);
            }
            if (value instanceof List<?>) {
                value = ((List<?>) value).stream().map(v -> {
                    if (v instanceof String) {
                        return substitute(v);
                    }
                    return v;
                }).collect(Collectors.toList());
            }

            when = when.queryParam(queryParam.getKey(), value);
        }

        return when;
    }

}
