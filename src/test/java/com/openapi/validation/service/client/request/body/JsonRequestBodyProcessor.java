package com.openapi.validation.service.client.request.body;

import static io.restassured.http.ContentType.ANY;
import static io.restassured.http.ContentType.JSON;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.domain.config.RequestSpec;
import com.openapi.validation.factory.schema.OpenApiObjectFactory;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.models.media.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * application/json content type request body preprocessor
 */
public class JsonRequestBodyProcessor implements ContentTypeBodyProcessor {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonRequestBodyProcessor.class);

    /**
     * Object mapper suited to convert java objects to json
     */
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    static {
        OBJECT_MAPPER.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    private final OpenApiObjectFactory schemaObjectFactory;

    public JsonRequestBodyProcessor(OpenApiObjectFactory schemaObjectFactory) {
        this.schemaObjectFactory = schemaObjectFactory;
    }

    /**
     * @return any and application/json content types
     */
    @Override
    public List<String> acceptableContentTypes() {
        List<String> contentTypes = new ArrayList<>();
        contentTypes.add(ANY.getAcceptHeader());
        contentTypes.addAll(Arrays.stream(JSON.getContentTypeStrings()).collect(Collectors.toList()));
        return contentTypes;
    }

    /**
     * Apply content type to request.
     *
     * @param contentType content type.
     * @param when        request specification.
     * @return updated request specification
     */
    @Override
    public RequestSpecification applyContentType(String contentType, RequestSpecification when) {
        return when.contentType(JSON);
    }

    /**
     * Processes request to match operation specs from the config.json and openApi specifications.
     *
     * @param mediaType Media type meta from the OpenApi specification
     * @param spec      Operation specs: requirements to request
     * @param when      rest assured request specification.
     * @return updated rest assured request specification.
     */
    @Override
    public RequestSpecification process(MediaType mediaType, OperationSpec spec, RequestSpecification when) {
        if (spec == null) {
            return when;
        }

        RequestSpec requestSpec = spec.getRequest();
        Object body;
        if (requestSpec == null || requestSpec.getBody() == null) {
            body = schemaObjectFactory.create(mediaType.getSchema());
        } else {
            body = requestSpec.getBody();
            if (body instanceof Map<?, ?> || body instanceof List<?>) {
                body = schemaObjectFactory.create(mediaType.getSchema(), body);
            }
        }

        try {
            String bodyString = OBJECT_MAPPER.writeValueAsString(body);
            when = when.body(bodyString);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return when;
    }
}
