package com.openapi.validation.service.client.request.body;

import static com.openapi.validation.OpenApiValidationConstants.FILE_DATA_PLACEHOLDER;
import static com.openapi.validation.factory.schema.OpenApiFakeSubstitutionFactory.substitute;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.domain.config.RequestSpec;
import com.openapi.validation.util.FileUtils;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.models.media.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * multipart/* content type request body preprocessor
 */
public class MultiPartBodyProcessor implements ContentTypeBodyProcessor {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MultiPartBodyProcessor.class);

    /**
     * @return multipart content types
     */
    @Override
    public List<String> acceptableContentTypes() {
        return Arrays.asList("multipart/form-data", "multipart/mixed");
    }

    /**
     * Apply content type to request.
     *
     * @param contentType content type.
     * @param when        request specification.
     * @return updated request specification
     */
    @Override
    public RequestSpecification applyContentType(String contentType, RequestSpecification when) {
        return when.contentType(contentType);
    }

    /**
     * Processes request to match operation specs from the config.json and openApi specifications.
     *
     * @param mediaType Media type meta from the OpenApi specification
     * @param spec      Operation specs: requirements to request
     * @param when      rest assured request specification.
     * @return updated rest assured request specification.
     */
    @Override
    public RequestSpecification process(MediaType mediaType, OperationSpec spec, RequestSpecification when) {
        if (spec == null) {
            return when;
        }

        RequestSpec requestSpec = spec.getRequest();
        if (requestSpec == null) {
            return when;
        }

        Object body = requestSpec.getBody();
        if (body == null) {
            return when;
        }

        try {
            if (body instanceof String) {
                when = when.multiPart((File) substituteValue((String) body));
            } else if (body instanceof Map<?, ?>) {
                //noinspection unchecked
                Map<String, Object> bodyMap = (Map<String, Object>) body;
                for (Entry<String, Object> entry : bodyMap.entrySet()) {
                    Object value = entry.getValue();
                    when = when.multiPart(entry.getKey(), value instanceof String ? substitute(value) : value);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return when;
    }

    private Object substituteValue(String value) throws IOException {
        if (!value.startsWith(FILE_DATA_PLACEHOLDER)) {
            return value;
        }

        String path = value.replaceFirst(FILE_DATA_PLACEHOLDER, "");
        return FileUtils.load(path);
    }

}
