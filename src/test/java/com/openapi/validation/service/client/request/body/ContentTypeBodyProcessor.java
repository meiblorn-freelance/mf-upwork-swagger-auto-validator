package com.openapi.validation.service.client.request.body;

import java.util.List;

import com.openapi.validation.domain.config.OperationSpec;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.models.media.MediaType;

public interface ContentTypeBodyProcessor {

    /**
     * @return Content types acceptable by processor.
     */
    List<String> acceptableContentTypes();

    /**
     * Apply content type to request specification (set Content-Type header).
     *
     * @param contentType content type.
     * @param when        request specification.
     * @return updated request specification.
     */
    RequestSpecification applyContentType(String contentType, RequestSpecification when);

    /**
     * Processes request to match operation specs from the config.json and openApi specifications.
     *
     * @param mediaType Media type meta from the OpenApi specification
     * @param spec      Operation specs: requirements to request
     * @param when      rest assured request specification.
     * @return updated rest assured request specification.
     */
    RequestSpecification process(MediaType mediaType, OperationSpec spec, RequestSpecification when);

}
