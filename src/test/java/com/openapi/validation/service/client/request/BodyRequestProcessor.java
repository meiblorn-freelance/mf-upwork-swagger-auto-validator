package com.openapi.validation.service.client.request;

import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.domain.config.RequestSpec;
import com.openapi.validation.factory.schema.OpenApiObjectFactory;
import com.openapi.validation.service.client.request.body.ContentTypeBodyProcessor;
import com.openapi.validation.service.client.request.body.FormRequestBodyProcessor;
import com.openapi.validation.service.client.request.body.JsonRequestBodyProcessor;
import com.openapi.validation.service.client.request.body.MultiPartBodyProcessor;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.parameters.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Processor suited to fill request body.
 */
public class BodyRequestProcessor implements RequestProcessor<RequestSpecification> {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BodyRequestProcessor.class);

    /**
     *
     */
    private List<ContentTypeBodyProcessor> contentTypeBodyProcessors;

    public BodyRequestProcessor(OpenAPI openApi) {
        contentTypeBodyProcessors = Arrays.asList(
                new JsonRequestBodyProcessor(
                        new OpenApiObjectFactory(openApi)
                ),
                new FormRequestBodyProcessor(),
                new MultiPartBodyProcessor()
        );
    }

    /**
     * Process method.
     *
     * @param path       url path
     * @param op         OpenApi operation
     * @param httpMethod Http method
     * @param spec       Operation specs
     * @param when       request specification root instance reference.
     * @return updated request specification root instance reference as passed to "when" parameters.
     */
    public RequestSpecification process(String path, Operation op, PathItem.HttpMethod httpMethod,
            OperationSpec spec, RequestSpecification when) {

        String contentType = null;
        if (spec != null) {
            RequestSpec requestSpec = spec.getRequest();
            if (requestSpec != null) {
                contentType = requestSpec.getContentType();
            }
        }

        return processContentType(contentType, op, spec, when);
    }

    private RequestSpecification processContentType(String contentType, Operation op, OperationSpec spec,
            RequestSpecification when) {

        RequestBody requestBody = op.getRequestBody();
        if (requestBody == null) {
            return when;
        }

        for (Entry<String, MediaType> entry : requestBody.getContent().entrySet()) {
            String currentContentType = entry.getKey();
            if (contentType != null && !contentType.equals(currentContentType)) {
                continue;
            }

            for (ContentTypeBodyProcessor contentTypeBodyProcessor : contentTypeBodyProcessors) {
                if (!contentTypeBodyProcessor.acceptableContentTypes().contains(currentContentType)) {
                    continue;
                }

                MediaType mediaType = entry.getValue();

                when = contentTypeBodyProcessor.applyContentType(contentType, when);
                when = contentTypeBodyProcessor.process(mediaType, spec, when);

                return when;
            }
        }

        return when;
    }

}
