package com.openapi.validation.service.client;

import static io.restassured.RestAssured.given;

import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.service.client.request.BodyRequestProcessor;
import com.openapi.validation.service.client.request.HeadersRequestProcessor;
import com.openapi.validation.service.client.request.PathParametersRequestProcessor;
import com.openapi.validation.service.client.request.QueryParametersRequestProcessor;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem.HttpMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Rest Assured client suited to build request and send it to the server.
 */
public class RestAssuredClient {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(RestAssuredClient.class);

    /**
     * Processes request body for request
     */
    private BodyRequestProcessor requestBodyProcessor;

    /**
     * Processes query parameters (e.g. ?a=4&b=5) for request
     */
    private QueryParametersRequestProcessor queryParametersProcessor;

    /**
     * Process path parameters (e.g. /{pet}/{orderId}) for request
     */
    private PathParametersRequestProcessor pathParametersProcessor;

    /**
     * Process headers for request
     */
    private HeadersRequestProcessor headersRequestProcessor;

    /**
     * Request initial state
     */
    private RequestSpecification given;


    public static Builder builder(OpenAPI openApi) {
        return new Builder(openApi);
    }

    /**
     * Form request and send it server.
     *
     * @param path       Sub-url / sub-path. Base url is provided by "given" field instance.
     * @param httpMethod HTTP method (get, post, and etc.).
     * @param op         Operation OpenApi specification.
     * @param spec       Operation specs: requirements needed to run and validate operation properly.
     * @return response from the server.
     */
    public Response call(String path, HttpMethod httpMethod, Operation op, OperationSpec spec) {
        RequestSpecification requestSpec = given != null ? given.when() : given();

        PathParametersRequestProcessor.Result pathParametersResult =
                pathParametersProcessor.process(path, op, httpMethod, spec, requestSpec);

        requestSpec = pathParametersResult.getWhen();
        requestSpec = headersRequestProcessor.process(path, op, httpMethod, spec, requestSpec);

        Response response;
        switch (httpMethod) {
            case HEAD:
            case OPTIONS:
            case TRACE:
            case GET:
                requestSpec = queryParametersProcessor.process(path, op, httpMethod, spec, requestSpec);
                response = requestSpec.get(path);
                break;
            case POST:
                requestSpec = requestBodyProcessor.process(path, op, httpMethod, spec, requestSpec);
                response = requestSpec.post(path);
                break;
            case PATCH:
            case PUT:
                requestSpec = requestBodyProcessor.process(path, op, httpMethod, spec, requestSpec);
                response = requestSpec.put(path);
                break;
            case DELETE:
                requestSpec = queryParametersProcessor.process(path, op, httpMethod, spec, requestSpec);
                response = requestSpec.delete(path);
                break;
            default:
                throw new RuntimeException("Unknown HTTP method");
        }

        return response;
    }

    /**
     * Builder to create client instance
     */
    public static final class Builder {

        private OpenAPI openApi;

        private RequestSpecification given;

        private Builder(OpenAPI openApi) {
            this.openApi = openApi;
        }

        public Builder withGiven(RequestSpecification given) {
            this.given = given;
            return this;
        }

        public RestAssuredClient build() {
            RestAssuredClient client = new RestAssuredClient();
            client.given = this.given;
            client.requestBodyProcessor = new BodyRequestProcessor(openApi);
            client.pathParametersProcessor = new PathParametersRequestProcessor();
            client.queryParametersProcessor = new QueryParametersRequestProcessor();
            client.headersRequestProcessor = new HeadersRequestProcessor();
            return client;
        }
    }
}
