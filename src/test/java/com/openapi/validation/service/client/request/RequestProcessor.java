package com.openapi.validation.service.client.request;

import com.openapi.validation.domain.config.OperationSpec;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;

/**
 * Processor interface suited to form request parameters.
 *
 * @param <T> processor return type
 */
public interface RequestProcessor<T> {

    /**
     * Process method.
     *
     * @param path       url path
     * @param op         OpenApi operation
     * @param httpMethod Http method
     * @param spec       Operation specs
     * @param when       request specification root instance reference.
     * @return value of type T
     */
    T process(String path, Operation op, PathItem.HttpMethod httpMethod, OperationSpec spec,
            RequestSpecification when);

}
