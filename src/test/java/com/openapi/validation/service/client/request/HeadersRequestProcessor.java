package com.openapi.validation.service.client.request;

import static com.openapi.validation.factory.schema.OpenApiFakeSubstitutionFactory.substitute;

import java.util.Map;

import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.domain.config.RequestSpec;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem.HttpMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Processor suited to fill request headers.
 */
public class HeadersRequestProcessor implements RequestProcessor<RequestSpecification> {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(HeadersRequestProcessor.class);

    /**
     * Process method.
     *
     * @param path       url path
     * @param op         OpenApi operation
     * @param httpMethod Http method
     * @param spec       Operation specs
     * @param when       request specification root instance reference.
     * @return updated request specification root instance reference as passed to "when" parameters.
     */
    public RequestSpecification process(String path, Operation op, HttpMethod httpMethod, OperationSpec spec,
            RequestSpecification when) {

        if (spec == null) {
            return when;
        }

        RequestSpec requestSpec = spec.getRequest();
        if (requestSpec == null) {
            return when;
        }

        Map<String, String> headers = requestSpec.getHeaders();
        if (headers == null || headers.isEmpty()) {
            return when;
        }

        if (op.getParameters() == null) {
            return when;
        }

        for (Map.Entry<String, String> header : headers.entrySet()) {
            when = when.header(header.getKey(), substitute(header.getValue()));
        }

        return when;
    }

}
