package com.openapi.validation.factory.schema;

import static com.openapi.validation.OpenApiValidationConstants.RANDOM_DATA_PLACEHOLDER;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import io.swagger.v3.oas.models.media.Schema;

@SuppressWarnings("WeakerAccess")
public class OpenApiFakeSubstitutionFactory {

    public static Object substitute(Object value) {
        return substitute(null, value);
    }

    public static Object substitute(Schema schema, Object value) {
        if (value instanceof List<?>) {
            List<Object> result = new ArrayList<>();
            for (Object subValue : ((List<?>) value)) {
                result.add(substitute(value));
            }
            return result;
        }

        if (value instanceof Map<?, ?>) {
            Map<Object, Object> result = new HashMap<>();
            for (Entry<?, ?> entry : ((Map<?, ?>) value).entrySet()) {
                result.put(entry.getKey(), substitute(entry.getValue()));
            }
            return result;
        }

        if (!(value instanceof String)) {
            return value;
        }

        if (RANDOM_DATA_PLACEHOLDER.equals(value)) {
            if (schema != null) {
                return OpenApiFakeDataFactory.fake(schema);
            }
            return String.valueOf(OpenApiFakeDataFactory.fakeString());
        }

        return value;
    }

}
