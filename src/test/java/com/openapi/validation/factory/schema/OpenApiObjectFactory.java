package com.openapi.validation.factory.schema;

import static com.openapi.validation.factory.schema.OpenApiFakeDataFactory.fake;
import static com.openapi.validation.factory.schema.OpenApiFakeSubstitutionFactory.substitute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.openapi.validation.util.SchemaUtils;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Object Factory. Generate objects maps from the Swagger component definition.
 */
@SuppressWarnings("WeakerAccess")
public class OpenApiObjectFactory {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenApiObjectFactory.class);

    private OpenAPI openApi;

    public OpenApiObjectFactory(OpenAPI openApi) {
        this.openApi = openApi;
    }

    public Object create(Schema schema) {
        return create(schema, new HashMap<>());
    }

    public Object create(Schema schema, Object values) {
        String ref = schema.get$ref();
        if (ref == null) {

            if (SchemaUtils.isArraySchema(schema) && values instanceof List<?>) {
                List<Object> result = new ArrayList<>();
                for (Object value : (List<?>) values) {
                    result.add(create(((ArraySchema) schema).getItems(), value));
                }
                return result;
            }

            if (values != null) {
                return substitute(schema, values);
            }
            return fake(schema);
        }
        ref = ref.substring(ref.lastIndexOf('/') + 1);
        return create(ref, values);
    }

    @SuppressWarnings("unchecked")
    private Object create(String ref, Object values) {
        Schema schema = this.openApi.getComponents().getSchemas().get(ref);

        Map<String, Object> valuesMap = (Map<String, Object>) values;
        Map<String, Schema> properties = schema.getProperties();
        Map<String, Object> objectMap = new HashMap<>();

        for (Map.Entry<String, Schema> entry : properties.entrySet()) {
            String propertyName = entry.getKey();
            if (!valuesMap.containsKey(propertyName)) {
                continue;
            }

            Object value = valuesMap.get(propertyName);
            Schema propertySchema = entry.getValue();

            objectMap.put(propertyName, substitute(propertySchema, value));
        }

        return objectMap;
    }

}
