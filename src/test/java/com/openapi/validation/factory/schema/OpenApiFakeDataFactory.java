package com.openapi.validation.factory.schema;

import java.util.List;
import java.util.Random;

import com.openapi.validation.util.SchemaUtils;
import io.codearte.jfairy.Fairy;
import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Fake data factory. Generates fake data based on Swagger scheme specs.
 */
@SuppressWarnings("WeakerAccess")
public class OpenApiFakeDataFactory {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenApiFakeDataFactory.class);

    private static Random random = new Random();

    private static Fairy fairy = Fairy.create();

    public static Object fake(Schema schema) {
        if (schema == null) {
            return null;
        }

        if (SchemaUtils.isStringSchema(schema)) {
            String value;
            if (schema.getDefault() != null) {
                value = (String) schema.getDefault();
            } else {
                List schemaEnum = schema.getEnum();
                if (schemaEnum != null) {
                    value = (String) schemaEnum.get(random.nextInt(schemaEnum.size()));
                } else {
                    value = fakeString();
                }
            }
            return value;
        }
        if (SchemaUtils.isIntegerSchema(schema)) {
            return fairy.baseProducer().randomBetween(0, 1000);
        }
        if (SchemaUtils.isBooleanSchema(schema)) {
            return random.nextBoolean();
        }
        if (SchemaUtils.isDateTimeSchema(schema)) {
            return fairy.dateProducer().randomDateInTheFuture().toString();
        }
        if (SchemaUtils.isArraySchema(schema)) {
            Schema itemsSchema = ((ArraySchema) schema).getItems();
            if (itemsSchema != null && (itemsSchema.getEnum() != null || itemsSchema.getDefault() != null)) {
                return fake(itemsSchema);
            }
            return new Object[0];
        }

        return null;
    }

    public static String fakeString() {
        return fairy.textProducer().word(1);
    }

}
