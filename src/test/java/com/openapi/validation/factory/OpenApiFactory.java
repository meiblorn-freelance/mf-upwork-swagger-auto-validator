package com.openapi.validation.factory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

import com.atlassian.oai.validator.OpenApiInteractionValidator;
import io.swagger.parser.OpenAPIParser;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.parser.core.models.AuthorizationValue;
import io.swagger.v3.parser.core.models.ParseOptions;
import io.swagger.v3.parser.core.models.SwaggerParseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory to fetch OpenApi specs from the remote origin.
 * It's copy of private method listed below
 *
 * @see OpenApiInteractionValidator#loadApi(java.lang.String, java.util.List)
 */
@SuppressWarnings("WeakerAccess")
public class OpenApiFactory {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenApiFactory.class);

    public static OpenAPI create(@Nonnull String specUrlOrPayload) {
        return create(specUrlOrPayload, null);
    }

    public static OpenAPI create(@Nonnull String specUrlOrPayload, @Nullable List<AuthorizationValue> authData) {
        OpenAPIParser openAPIParser = new OpenAPIParser();
        ParseOptions parseOptions = new ParseOptions();
        parseOptions.setResolve(true);
        parseOptions.setResolveFully(true);
        parseOptions.setResolveCombinators(false);

        SwaggerParseResult parseResult;
        try {
            parseResult = openAPIParser.readLocation(specUrlOrPayload, authData, parseOptions);
            if (parseResult == null || parseResult.getOpenAPI() == null) {
                parseResult = openAPIParser.readContents(specUrlOrPayload, authData, parseOptions);
            }
        } catch (Exception e) {
            throw new RuntimeException(specUrlOrPayload, e);
        }

        if (parseResult != null && parseResult.getOpenAPI() != null
                && (parseResult.getMessages() == null || parseResult.getMessages().isEmpty())) {
            return parseResult.getOpenAPI();
        } else {
            throw new RuntimeException(specUrlOrPayload);
        }
    }

}
