package com.openapi.validation.factory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.openapi.validation.domain.config.Config;
import com.openapi.validation.domain.config.EndpointDefinition;
import com.openapi.validation.domain.config.HttpMethodDefinition;
import com.openapi.validation.domain.config.OperationDefinition;
import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.util.OpenApiUtils;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates tests data (server url, paths, httpMethod, specs) from the provided config and openApi instance.
 */
public class OpenApiValidationTestDataFactory {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenApiValidationTestDataFactory.class);

    /**
     * Creates test data.
     *
     * @param config  config from config.json.
     * @param openApi openApi from the server.
     * @return tests data.
     */
    public static Object[][] create(Config config, OpenAPI openApi) {
        List<Object[]> resultList = new ArrayList<>();
        for (String server : OpenApiUtils.servers(openApi)) {
            resultList.addAll(createForServer(config, openApi, server));
        }

        Object[][] result = new Object[resultList.size()][];
        for (int i = 0; i < resultList.size(); i++) {
            result[i] = resultList.get(i);
        }

        return result;
    }

    /**
     * Creates test data for particular server.
     *
     * @param config  config from config.json.
     * @param openApi openApi from the server.
     * @return tests data.
     */
    private static List<Object[]> createForServer(Config config, OpenAPI openApi, String server) {
        List<Object[]> result = new ArrayList<>();

        for (Entry<String, PathItem> entry : openApi.getPaths().entrySet()) {
            String path = entry.getKey();
            PathItem pathItem = entry.getValue();

            List<EndpointDefinition> endpoints =
                    config.getEndpoints().stream().filter(e -> e.getPath().equals(path))
                          .collect(Collectors.toList());

            Map<PathItem.HttpMethod, Operation> operations = pathItem.readOperationsMap();
            if (!endpoints.isEmpty()) {
                for (EndpointDefinition endpoint : endpoints) {
                    result.addAll(createForPath(server, path, operations, endpoint));
                }
            } else {
                result.addAll(createForPath(server, path, operations, null));
            }
        }

        return result;
    }

    /**
     * Creates test data for particular server path.
     *
     * @param path       path url.
     * @param operations operations meta.
     * @param endpoint   endpoint from config.json.
     * @return tests data.
     */
    private static List<Object[]> createForPath(String server, String path,
            Map<PathItem.HttpMethod, Operation> operations, EndpointDefinition endpoint) {
        List<Object[]> result = new ArrayList<>();

        List<OrderedData> orderedData = new ArrayList<>();
        for (Entry<PathItem.HttpMethod, Operation> operationEntry : operations.entrySet()) {
            PathItem.HttpMethod httpMethod = operationEntry.getKey();
            Operation operation = operationEntry.getValue();

            if (endpoint == null) {
                result.add(new Object[]{server, path, httpMethod, operation, null});
                continue;
            }

            HttpMethodDefinition httpMethodDefinition = HttpMethodDefinition.forValue(httpMethod.name());
            OperationDefinition operationDefinition =
                    endpoint.getOperations().getOrDefault(httpMethodDefinition, null);

            if (operationDefinition == null) {
                continue;
            }

            for (OperationSpec operationSpec : operationDefinition.getSpecs()) {
                orderedData.add(
                        new OrderedData(
                                operationDefinition.getOrder(),
                                new Object[]{server, path, httpMethod, operation, operationSpec}
                        )
                );
            }
        }

        if (!orderedData.isEmpty()) {
            orderedData.sort(Comparator.comparingInt(o -> o.order));
            for (OrderedData item : orderedData) {
                result.add(item.data);
            }
        }

        return result;
    }

    private static class OrderedData {

        private int order;

        private Object[] data;

        OrderedData(int order, Object[] data) {
            this.order = order;
            this.data = data;
        }
    }

}
