package com.openapi.validation;

import static com.openapi.validation.OpenApiValidationConstants.ALLOWED_STATUS_CODES;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.isIn;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.oai.validator.OpenApiInteractionValidator;
import com.openapi.validation.domain.config.Config;
import com.openapi.validation.domain.config.OperationSpec;
import com.openapi.validation.domain.config.ResponseSpec;
import com.openapi.validation.factory.OpenApiFactory;
import com.openapi.validation.factory.OpenApiValidationTestDataFactory;
import com.openapi.validation.filter.ValidationFilterProxy;
import com.openapi.validation.service.client.RestAssuredClient;
import com.openapi.validation.util.ConfigUtils;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem.HttpMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class OpenApiValidationTest implements ITest {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(OpenApiValidationTest.class);

    /**
     * Holds generated test name
     */
    private ThreadLocal<String> testName = new ThreadLocal<>();

    /**
     * Config to configure tests.
     */
    private Config config;

    /**
     * OpenApi is needed there to load all the endpoints from schema.
     */
    private OpenAPI openApi;

    /**
     * OpenApi interaction validator provided by "swagger-request-validator-core" library.
     */
    private OpenApiInteractionValidator validator;

    /**
     * Tests preparation method. Heart of the this test suite.
     * Here we are reading config from the *.json file and initializing openApi connection.
     *
     * @throws IOException when config file is inaccessible.
     */
    @BeforeClass
    public void setUp() throws IOException {
        Config config = ConfigUtils.read();
        String swaggerUrl = config.getUrl();

        this.config = config;
        this.openApi = OpenApiFactory.create(swaggerUrl);
        this.validator = OpenApiInteractionValidator.createFor(swaggerUrl).build();
    }

    /**
     * Method suited for generation of human-readable test name.
     */
    @BeforeMethod
    public void beforeMethod(Method method, Object[] testData) {
        String serverName = (String) testData[0];
        String path = (String) testData[1];
        HttpMethod httpMethod = (HttpMethod) testData[2];
        testName.set(httpMethod.name() + ": " + serverName + path + " ");
    }

    /**
     * @return Human-readable test name.
     */
    @Override
    public String getTestName() {
        return testName.get();
    }

    /**
     * Data provider for TestNG.
     *
     * @return tests data for all the openApi endpoints and specs from the config.
     */
    @DataProvider(name = "openApiPaths")
    public Object[][] openApiPathsDataProvider() {
        return OpenApiValidationTestDataFactory.create(config, openApi);
    }

    /**
     * Another heart of test suite.
     * TestNG entry point to start and run tests against swagger specifications.
     *
     * @param server     Server base-url
     * @param path       Endpoints sub-url
     * @param httpMethod Http method: Get, Post and etc.
     * @param op         OpenApi/Swagger operation instance. Holds operations meta information
     * @param spec       Operation spec: requirements needed to run and validate operation properly
     */
    @Test(dataProvider = "openApiPaths")
    public void testHttpMethod(String server, String path, HttpMethod httpMethod, Operation op,
            OperationSpec spec) {
        LOGGER.info("Testing '{} {}' endpoint", httpMethod, path);

        /*
            Pre initialize request
         */
        RequestSpecification given = given()
                .baseUri(server)
                .filter(new RequestLoggingFilter())
                .filter(new ResponseLoggingFilter())
                .filter(new ErrorLoggingFilter())
                .filter(new ValidationFilterProxy(validator, spec));

        /*
            Initialize client to send request
         */
        RestAssuredClient client = RestAssuredClient
                .builder(openApi)
                .withGiven(given)
                .build();

        /*
            Form and send request
         */
        Response response = client.call(path, httpMethod, op, spec);

        /*
            Initialize validation object
         */
        ValidatableResponse then = response.then();

        /*
            Validate status codes
         */
        validateStatusCodes(spec, then);
    }

    private void validateStatusCodes(OperationSpec spec, ValidatableResponse then) {
        List<Integer> allowedStatusCodes = new ArrayList<>(ALLOWED_STATUS_CODES);
        if (spec != null) {
            ResponseSpec responseSpec = spec.getResponse();
            if (responseSpec != null) {
                allowedStatusCodes.addAll(responseSpec.getStatus());
            }
        }
        then.assertThat().statusCode(isIn(allowedStatusCodes));
    }

}
